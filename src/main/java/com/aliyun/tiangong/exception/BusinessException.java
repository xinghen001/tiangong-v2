package com.aliyun.tiangong.exception;

/**
 * @author xinghen
 * @ClassName: BusinessException
 * @Package: com.aliyun.tiangong.exception
 * @Description 自定义业务异常
 * @create 2018/04/20 06:37
 */
public class BusinessException extends RuntimeException {

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
