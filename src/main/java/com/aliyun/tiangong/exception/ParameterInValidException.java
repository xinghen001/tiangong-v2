package com.aliyun.tiangong.exception;

/**
 * @author xinghen
 * @ClassName: ParameterInValidException
 * @Package: com.xinghen.common.core.exception
 * @Description 自定义参数校验异常
 * @create 2018/4/18 16:07
 */
public class ParameterInValidException extends RuntimeException {

    public ParameterInValidException() {
        super();
    }

    public ParameterInValidException(String message) {
        super(message);
    }

    public ParameterInValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParameterInValidException(Throwable cause) {
        super(cause);
    }

    protected ParameterInValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
