package com.aliyun.tiangong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xinghen
 * @ClassName: TiangongApplication
 * @Package: com.aliyun
 * @Description 启动类
 * @create 2018/4/19 20:35
 */
@RestController
@SpringBootApplication
@MapperScan(basePackages = "com.aliyun.tiangong.mapper")
public class TiangongApplication {

    @PostMapping("/test")
    public void test(@RequestBody Map<String, Object> map, HttpServletRequest request) {
        System.out.println(map);
        System.out.println(request);
    }

    public static void main(String[] args) {
        SpringApplication.run(TiangongApplication.class, args);
    }
}
