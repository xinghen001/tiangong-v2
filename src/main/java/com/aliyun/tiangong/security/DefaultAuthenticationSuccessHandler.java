package com.aliyun.tiangong.security;

import com.aliyun.tiangong.constant.TiangongConstant;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xinghen
 * @ClassName: DefaultAuthenticationSuccessHandler
 * @Package: com.aliyun.tiangong.security
 * @Description 认证成功处理器
 * @create 2018/04/20 23:35
 */
@Component
public class DefaultAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        super.onAuthenticationSuccess(request, response, authentication);
        User user = (User) authentication.getPrincipal();
        request.getSession().setAttribute(TiangongConstant.CURRENT_USER, user);
    }
}
