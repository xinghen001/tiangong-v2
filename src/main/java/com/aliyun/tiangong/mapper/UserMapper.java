package com.aliyun.tiangong.mapper;

import com.aliyun.tiangong.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author xinghen
 * @ClassName: UserMapper
 * @Package: com.aliyun.tiangong.mapper
 * @Description user mapper
 * @create 2018/04/26 06:56
 */
@Mapper
public interface UserMapper extends BaseMapper {

    @Select("select password from user where loginName = #{loginName}")
    List<String> selectPasswordByLoginName(String loginName);

}
