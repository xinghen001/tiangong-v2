package com.aliyun.tiangong.controller;

import com.aliyun.tiangong.support.RestResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xinghen
 * @ClassName: UserController
 * @Package: com.aliyun.tiangong.controller
 * @Description 用户处理器
 * @create 2018-04-20 15:30
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/test.json")
    public RestResult test() {
        return RestResult.ok();
    }

}
