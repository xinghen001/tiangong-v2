package com.aliyun.tiangong.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author xinghen
 * @ClassName: PageController
 * @Package: com.aliyun.tiangong.controller
 * @Description 页面跳转处理器
 * @create 2018-04-20 15:27
 */
@Controller
public class PageController {

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping({"/", "/index"})
    public String index() {
        return "test";
    }

    /**
     * 登陆页面
     *
     * @return
     */
    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

    /**
     * ci链路页面
     *
     * @return
     */
    @RequestMapping("/commit-info")
    public String commitInfoPage() {
        return "commit-info";
    }

}
