package com.aliyun.tiangong.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author xinghen
 * @ClassName: BaseMapper
 * @Package: com.aliyun.tiangong.base
 * @Description 通用Mapper接口，已实现基本的增、删、改、查方法
 * @create 2018/4/20 06:36
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
