package com.aliyun.tiangong.base.impl;

import com.aliyun.tiangong.base.IService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @author xinghen
 * @ClassName: BaseService
 * @Package: com.aliyun.tiangong.base.impl
 * @Description 通用Service实现
 * @create 2018-04-20 11:23
 */
public class BaseService<T> implements IService<T> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Mapper<T> mapper;

    @Override
    public T getByPrimaryKey(Object key) {
        return mapper.selectByPrimaryKey(key);
    }

    @Override
    public int save(T data) {
        return mapper.insertSelective(data);
    }

    @Override
    public int update(T data) {
        return mapper.updateByPrimaryKeySelective(data);
    }

    @Override
    public int delete(Object key) {
        return mapper.deleteByPrimaryKey(key);
    }

    @Override
    public List<T> getAll() {
        return mapper.selectAll();
    }

    @Override
    public PageInfo<T> getByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<T> data = mapper.selectAll();
        return new PageInfo<>(data);
    }

}
