package com.aliyun.tiangong.base;

import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author xinghen
 * @ClassName: IService
 * @Package: com.aliyun.tiangong.base
 * @Description Service通用接口
 * @create 2018-04-20 11:20
 */
public interface IService<T> {

    /**
     * 通过主键获取数据
     *
     * @param key
     * @return
     */
    T getByPrimaryKey(Object key);

    /**
     * 保存数据
     *
     * @param data
     * @return
     */
    int save(T data);

    /**
     * 更新数据
     *
     * @param data
     * @return
     */
    int update(T data);

    /**
     * 删除数据
     *
     * @param key
     * @return
     */
    int delete(Object key);

    /**
     * 获取所有数据
     *
     * @return
     */
    List<T> getAll();

    /**
     * 分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<T> getByPage(int pageNum, int pageSize);

}
