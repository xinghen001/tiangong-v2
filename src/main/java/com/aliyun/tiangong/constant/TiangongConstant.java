package com.aliyun.tiangong.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author xinghen
 * @ClassName: TiangongConstant
 * @Package: com.aliyun.tiangong.constant
 * @Description 常量
 * @create 2018/04/20 23:08
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TiangongConstant {

    /**
     * rest api url
     */
    public static final String JSON_URL_SUFFIX = ".json";

    /**
     * session中存放登陆用户的key
     */
    public static final String CURRENT_USER = "currentUser";

}
