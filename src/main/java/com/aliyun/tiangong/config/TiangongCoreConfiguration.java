package com.aliyun.tiangong.config;

import com.aliyun.tiangong.config.properties.TiangongProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author xinghen
 * @ClassName: TiangongCoreConfiguration
 * @Package: com.aliyun.config
 * @Description 天工核心配置
 * @create 2018/04/19 21:24
 */
@Configuration
@EnableConfigurationProperties(TiangongProperties.class)
public class TiangongCoreConfiguration {
}
