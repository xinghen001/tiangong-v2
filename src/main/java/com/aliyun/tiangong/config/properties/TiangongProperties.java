package com.aliyun.tiangong.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xinghen
 * @ClassName: TiangongProperties
 * @Package: com.aliyun.config.properties
 * @Description 项目总配置
 * @create 2018/04/19 21:17
 */
@Data
@ConfigurationProperties(prefix = "tiangong")
public class TiangongProperties {

    private DruidProperties druid = new DruidProperties();

}
