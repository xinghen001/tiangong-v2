package com.aliyun.tiangong.config.properties;

import lombok.Data;

/**
 * @author xinghen
 * @ClassName: DruidProperties
 * @Package: com.aliyun.config
 * @Description druid配置
 * @create 2018/04/19 20:40
 */
@Data
public class DruidProperties {

    /**
     * 数据库驱动
     */
    private String driverClassName;

    /**
     * 数据库URL
     */
    private String url;

    /**
     * 数据库用户名
     */
    private String username;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 初始化时建立物理连接的个数
     */
    private Integer initialSize;

    /**
     * 最小连接池数量
     */
    private Integer minIdle;

    /**
     * 最大连接池数量
     */
    private Integer maxActive;

    /**
     * 获取连接时最大等待时间，单位毫秒
     */
    private Integer maxWait;

    /**
     * 销毁线程会检测连接的间隔时间
     */
    private Integer timeBetweenEvictionRunsMillis;

    /**
     * 销毁线程中如果检测到当前连接的最后活跃时间和当前时间的差值大于minEvictableIdleTimeMillis，
     * 则关闭当前连接
     */
    private Integer minEvictableIdleTimeMillis;

    /**
     * 用来检测连接是否有效的sql，要求是一个查询语句
     * 如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会其作用
     */
    private String validationQuery;

    /**
     * 建议配置为true，不影响性能，并且保证安全性。
     * 申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，
     * 执行validationQuery检测连接是否有效。
     */
    private Boolean testWhileIdle;

    /**
     * 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
     */
    private Boolean testOnBorrow;

    /**
     * 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
     */
    private Boolean testOnReturn;

    /**
     * 是否缓存preparedStatement，也就是PSCache
     */
    private Boolean poolPreparedStatements;

    private Integer maxPoolPreparedStatementPerConnectionSize;

    /**
     * 属性类型是字符串，通过别名的方式配置扩展插件，
     * 常用的插件有：
     * 监控统计用的filter:stat
     * 日志用的filter:log4j
     * 防御sql注入的filter:wall
     */
    private String filters;

    /**
     * 其他配置
     */
    private String connectionProperties;

    private DruidAdminProperties admin = new DruidAdminProperties();

    /**
     * druid监控台配置
     */
    @Data
    public class DruidAdminProperties {

        /**
         * 白名单IP,多个以逗号隔开
         */
        private String allow;

        /**
         * 黑名单IP,多个以逗号隔开
         */
        private String deny;

        /**
         * 登陆用户名
         */
        private String loginUsername;

        /**
         * 登陆密码
         */
        private String loginPassword;

        /**
         * 是否可以重置数据
         */
        private String resetEnable;

    }
}
