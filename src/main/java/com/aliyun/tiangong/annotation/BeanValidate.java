package com.aliyun.tiangong.annotation;

import com.aliyun.tiangong.base.BaseEntity;

import java.lang.annotation.*;

/**
 * @author xinghen
 * @ClassName: BeanValidate
 * @Package: com.xinghen.common.core.annotation
 * @Description 方法入参校验注解
 * @create 2018-04-11 15:36
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BeanValidate {

    /**
     * 要校验的参数类型
     *
     * @return
     */
    Class<?> validateClass() default BaseEntity.class;

}
